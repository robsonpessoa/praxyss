/*    Hello World using memory mapped IO video.


*/


#include "mbr.h"
#include "fnct.h"

#define _(...) __asm__(# __VA_ARGS__ "\n\t")

/* This will be our entry point */

int main(void)           
{
  
  bios_print ("This was written by bios\r\n");

  /* Rene, why the heck this works. */
  
  _(movl $0xb8000, %edi);
  _(movb $'A', (%edi));
  _(movb $0x20, 1(%edi));

  /* while this does not work? */
  
  _(movw 0xb800, %bx);
  _(movw 0x0000, %di);
  _(movb $'B',   (%bx,%di));
  _(movb $0x20, 1(%bx,%di));

  
  halt();

  return 0;
}


/*  Notes.

	 
 */

