/*    Hello World from real-mode memmory mapped video.

      Same as mmio-03.c, printing a string.
*/


#include "mbr.h"
#include "fnct.h"


/* This will be our entry point */

int main(void)           
{
  bios_print ("Hello from BIOS!\r\n");

  mem_print  ("Hello from MMIO video!", 0x20);
  
  halt();

  return 0;
}


/*  Notes.

    Now we encapsulated everyting as functions and have two different versions
    we can use in pure C syntax. 

    C'mon, let's admit... C is awsome!

    Problem is: we are running out of space in our mere 512 byte long MBR!!!

    Just run

      $ hexdump -vC <file>

    and see there is no much room before the end of the block. Even worse
    mmio-04.bin does occupie all 512 bytes! Not even a single byte left!

    It's time to think of having the bootloader do its job: load the rest of 
    or binary code into RAM.

*/




