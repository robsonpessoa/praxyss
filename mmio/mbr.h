#ifndef MBR_H
#define MBR_H

/* Low-level functions. */

#define bios_int(interrupt) __asm__("int $" #interrupt)

#define set_l(r32,val) (r32 =  ((r32 & 0xffffff00) |  (val & 0x000000ff)))
#define set_h(r32,val) (r32 = ((r32 & 0xffff00ff) | ((val<<8) & 0x0000ff00)))
#define get_l(r32) (r32 & 0x000000ff)
#define get_h(r32) ((r32 & 0x0000ff00) >> 8)

#define asm_halt() __asm__("hlt")

#define seg_to_lin(segment,offset) ((segment << 4) + offset)


/* Types and attributes. */

#define NAKED __attribute__ ((naked))

#define r32_t register volatile int 

/* General purpose 32-bit registers. */

#define SET_EAX __asm__ ("eax")
#define SET_EBX __asm__ ("ebx")
#define SET_ECX __asm__ ("ecx")
#define SET_EDX __asm__ ("edx")

#define SET_EBP __asm__ ("ebp")
#define SET_ESI __asm__ ("esi")
#define SET_EDI __asm__ ("edi")
#define SET_ESP __asm__ ("esp")

/* Sgement 16-bit registers. */

#define SET_CS __asm__ ("cs")
#define SET_DS __asm__ ("ds")
#define SET_SS __asm__ ("ss")
#define SET_ES __asm__ ("es")
#define SET_FS __asm__ ("fs")
#define SET_GS __asm__ ("gs")


/* High level functions. */



/* Function main (called by _start). */

int main();


#endif	/* MBR_H */

