/*    Using memory mapped IO video.

      Same as mmio-02.c, but in pure C.
*/


#include "mbr.h"
#include "fnct.h"

#define VIDEO_MEM seg_to_lin(0xb800, 0x0000);

/* This will be our entry point */

int main(void)           
{
  char *v;
  
  bios_print ("Hello from BIOS!\r\n");

  v = (char *) VIDEO_MEM;	/* Sinful in userland. */

  v[0] = 'A';
  v[1] = 0x20;
  
  halt();

  return 0;
}


/*  Notes.

    We don't really need to care about registers; we're writing to memory!

    Any good old array of chars is all that we need. :-)

*/




