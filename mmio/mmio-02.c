/*    Using memory mapped IO video.

      Same as mmio-01.c, but in C.
*/


#include "mbr.h"
#include "fnct.h"

#define VIDEO_MEM seg_to_lin(0xb800, 0x0000);

/* This will be our entry point */

int main(void)           
{
  r32_t p SET_EDI;
    
  bios_print ("This was written by bios\r\n");

  /* C equivalent. */              /*  Underling assembly. */
  
  p = VIDEO_MEM                    /* _(movl $0xb8000, %edi); */
  *(char*) p = 'A';                /* _(movb $'A', (%edi)); */
  *(char*) (p+1) = 0x20;           /* _(movb $0x20, 1(%edi)); */
  
  halt();

  return 0;
}


/*  Notes.

    This examples uses mbr.h macros. Not terrifically better than inline asm,
    but serves the purpose of showing the step-by-step construction.

*/




