/*    Hello World from real-mode memmory mapped video.

      Same as mmio-03.c, printing a string.
*/


#include "mbr.h"
#include "fnct.h"

#define VIDEO_MEM seg_to_lin(0xb800, 0x0000);

const char string[] = "Hello World from memory video!";

/* This will be our entry point */

int main(void)           
{
  char *v = (char *) VIDEO_MEM;
  int i,j;
  
  bios_print ("Hello from BIOS!\r\n");

  i = 0;
  j = 0;
  while (string[i])
    {
      v[j++] = string[i++];
      v[j++] = 0x20;
    }
  
  halt();

  return 0;
}


/*  Notes.

    We can write strings.
*/




