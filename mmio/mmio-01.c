/*    Using memory mapped IO video (inline assembly).


*/


#include "mbr.h"
#include "fnct.h"

#define _(...) __asm__(# __VA_ARGS__ "\n\t")

/* This will be our entry point */

int main(void)           
{
  
  bios_print ("This was written by bios\r\n");

    
  /* In real mode, memory has a 20bit effective linear address which is
     actually segmented in 64k blocks. The x86 architectures specifies that

          address = (segment << 4)   + offset 
                  = (segment * 0x10) + offset

     wehre 'segment' and 'offset' are 16-bit registers (20bits means 1Mbyte).

     Eg: 0x6EF:0x1234 = 0x8124      (notice: segment+offset = 0x1923 != 0x8124)

         0000 0110 1110 1111 0000   segment << 4   0x6EF0
       +      0001 0010 0011 0100   Offset         0x1234
         ------------------------               	
         0000 1000 0001 0010 0100   Address        0x8124

*/

  /* Color video memory starts at 0xb800:0000, equivalent to effec. 0x8b000 */
  
  _(movl $0xb8000, %edi);
  _(movb $'A', (%edi));
  _(movb $0x20, 1(%edi));

  
  halt();

  return 0;
}


/*  Notes.

    This example uses inline assembly to make it clear what is happening on
    the background. 
 */

