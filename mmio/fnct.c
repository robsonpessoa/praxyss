
#include "mbr.h"

/* A 'print' function */

void  bios_print (const char *str)
{
  int i;
  r32_t eax SET_EAX;
  
  eax = 0x0;
  set_h(eax,0x0e);
  
  i = 0;
  while (str[i] != 0)
    {
      set_l(eax, str[i] );
      bios_int(0x10);
      i++;
    }

}

void mem_print (const char *str, char mode)
{
  short *v = (short *) seg_to_lin(0xb800, 0x0000);
  int i;
  
  i = 0;
  while (str[i])
    {
      /* v[i] = (str[i] << 8) + mode; */
      v[i] = (mode << 8) + str[i];
      ++i;
    }
}

void halt()
{
  asm_halt();
}
