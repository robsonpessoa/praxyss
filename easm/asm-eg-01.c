/* asm-eg-01.c - Example of extended assembly. */

#include <stdio.h>

int   main(void)
{
  int foo = 0x1;

  printf ("a = %d\n", foo);


  /* This is semantically equivalent to

     __register__ int foo __asm__("eax");

     foo = 2;                              */

  __asm__("mov $0x2, %%eax"
  	  : "=a" (foo)		
  	  :
  	  :
  	  );

  printf ("a = %d\n", foo);


  return 42;
} 


/* Note:

   Disassemble the produced machine code 

        $ objdump -d asm-eg-01.o

   and compare it to the equivalent

       foo = 2;

   Notice that foo was actually stored in eax.

*/

















  /* Syntax

     __asm__ ( asm template
               : input operands           
	       : output operands
	       : clobbered registers
	     )
 */
