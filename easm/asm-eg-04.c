/* asm-eg-01.c - Example of extended assembly. */

#include <stdio.h>

int   main(void)
{
  int foo = 0x1;
  int bar = 0x1;
  
  printf ("a = %d\n", foo);


  /* This is semantically equivalent to

     foo |= bar << 8;                          */

  __asm__("xor %%eax,%%eax;"
	  "movb %1, %%ah;" 
          "or %%eax, %0" 
  	  : "=m" (foo)  		
  	  :  "m" (bar)
  	  : "eax"
  	  );

  printf ("a = %d\n", foo);


  return 42;
} 


/* Note:

   The inline assembly clobbers eax, which is not listed as input/output 
   parameters, leaving gcc clueless. Therefore we have to tell the compiler
   not to trust eax contents after our asm code.

*/

