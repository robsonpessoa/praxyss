/* asm-eg-01.c - Example of extended assembly. */

#include <stdio.h>

int   main(void)
{
  int foo = 0x1;

  printf ("a = %d\n", foo);


  /* This is semantically equivalent to

     int foo;

     foo = 2;                              */

  __asm__("movl $0x2, %0" 
  	  : "=m" (foo)		/* %0 is *(&foo) */
  	  :
  	  :
  	  );

  printf ("a = %d\n", foo);

  return 42;
} 


/* Note:

   Disassemble the produced machine code 

        $ objdump -d asm-eg-01.o

   and compare it to asm-eg-01.c. Now foo is a memory location.


   Notice, however that, although semantically equivalent to

       foo = 2;

   the produced machine code is different from what we would get 
   with the pure C source code. The more inline assembly we have, the less
   freedom the C compiler has to take decisions and, consequently, to
   optimize the final object code. 

*/

















  /* Syntax

     __asm__ ( asm template
               : input operands           
	       : output operands
	       : clobbered registers
	     )
 */
