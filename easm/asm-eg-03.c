/* asm-eg-01.c - Example of extended assembly. */

#include <stdio.h>

int   main(void)
{
  int foo = 0x1;
  int bar = 0x2;
  
  printf ("a = %d\n", foo);


  /* This is semantically equivalent to

     foo = bar;                              */

  __asm__("movl %1, %%eax; movl %%eax, %0" 
  	  : "=m" (foo)  		
  	  :  "m" (bar)
  	  :
  	  );

  printf ("a = %d\n", foo);


  return 42;
} 


/* Note:

   Disassemble the produced machine code 

        $ objdump -d asm-eg-01.o

   and compare it to asm-eg-01.c and asm-eg-02.c 


*/

