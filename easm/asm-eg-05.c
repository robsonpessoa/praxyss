/* asm-eg-01.c - Example of extended assembly. */

#include <stdio.h>

#define set_al(val)  __asm__("movb %0, %%al"::"m"(val));
#define set_ah(val)  __asm__("movb %0, %%ah"::"m"(val));
#define set_ax(val)  __asm__("movw %0, %%ax"::"m"(val));
#define set_eax(val) __asm__("movl %0, %%eax"::"m"(val));

#define get_al(ptr)  __asm__("movb %%al, %0":"=m"(*ptr):);
#define get_ah(ptr)  __asm__("movb %%ah, %0":"=m"(*ptr):);
#define get_ax(ptr)  __asm__("movw %%ax, %0":"=m"(*ptr):);
#define get_eax(ptr) __asm__("movl %%eax, %0":"=m"(*ptr):);


int   main(void)
{
  int foo=1;
  int bar;
  
  __asm__("movl $0x0, %eax");

  /* Set al */


  set_ah (foo);
  get_eax(&bar);
  
  printf ("a = %d\n", bar);


  return 42;
} 


/* Note:

   The inline assembly clobbers eax, which is not listed as input/output 
   parameters, leaving gcc clueless. Therefore we have to tell the compiler
   not to trust eax contents after our asm code.

*/

