#ifndef PRAXYSS_FOO_H
#define PRAXYSS_FOO_H

/**
 * Receives two integer numbers and sums it.
 * @param a the first number
 * @param b the second number
 * @return the sum of a with b.
 */
int foo(int, int);

#endif //PRAXYSS_FOO_H
