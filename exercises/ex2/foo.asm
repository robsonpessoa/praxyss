    global foo
    section .text

foo:
    ; prologue (saves the current address of top of the stack into the ebp register)
    push   ebp
    mov    ebp,esp
    ; points the top of the stack to the size to 2 words of size 0x4.
    ; That means two words before the call instruction was made by the caller.
    add    esp,0x8
    ; saves those two words into the eax and ebx registers
    pop    eax
    pop    ebx
    ; adds ebx to eax (sums one register with the another and saves it in the eax register)
    add    eax,ebx
    ; epilogue (takes back the previous address the esp register from the ebp)
    mov    esp,ebp
    pop    ebp
    ; returns
    ret
