/* main.c - Exercise one. 

   Copyright (c) 2018 - Monaco F. J. <monaco@usp.br>

   This program is free open source software distrubted under GNU GPL vr.
   Please, see enclosed file COPYING for firther information.


   This program is intended to be built in a standrd GNU/Linux platform with

   gcc main.c -m32 -lfoobar - o main.

*/


#include <stdio.h>
#include <stdlib.h>

int foo(int);
int bar(int, int);  

void check_args (int);


/* Main. */

int main(int argc, char **argv)
{
  long int a, b, n;

  /* Read command line arguments. */

  check_args(argc);
  n = atol(argv[1]);
  
  /* Refer to foo.asm and verify that foo works as expected. */
  
  a = foo(1);			

  printf ("a = %ld\n", a);

  /* Refere to bar.asm and verify that it apparently works as expected
     for n 10, 100, 1000... However, the program fails for very large n,
     say n = 10000000 and beyond. 

     Using your knowledge on x86 calling convention, explay why this is
     happening. Consider that main.c is compiled with gcc on a standard GNU/Linux 
     host, fix the but in the assembly code. */
  
  b = bar (1,n);		  
  printf ("foobar = %ld\n", b);

  return 0;
}


/* Check command line arguments. */

void check_args(int nargs)
{
  if (nargs < 2)
    {
      fprintf (stderr, "Usage: main <integer_value>\n");
      exit (1);
    }
}
