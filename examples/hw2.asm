	;; Hello x86 assembly world 
        ;; With main

        global _start
        global main
        
        section .data
        msg db "Hello World", 0xa
        
        section .text

_start:
        call main

        mov ebx, eax            ; return status code
        mov eax, 1              ; sycal exit is #1
        int 0x80                ; perform syscall

main:
        mov eax, 4              ; syscall write
        mov ebx, 1              ; file descriptor
        mov ecx, msg            ; start address
        mov edx, 12             ; how many bytes
        int 0x80                ; perform syscall

        mov eax, 0
        ret
