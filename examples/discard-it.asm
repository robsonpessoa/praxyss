      ;; A minimal c-like example with nested function call

	global _start          ; Mandatory label for entry point           

	section .text          ; Program code segment                      

_start:                       ; The entry point starts here                            

        call main             ; push esp, then jmp main
        mov ebx, eax          ; get mains's return value from eax 

        mov eax, 1            ; Prepare for syscall exit 
        int 0x80              ; Perform syscall                                        

main:
	call func
        ret                    ; pop esp, then jmp esp

func:
	mov eax, 42
	ret
