	;; Sum up two arguments

	global sum

	section .text

sum:

	mov eax, ecx
	add eax, edx

	ret
