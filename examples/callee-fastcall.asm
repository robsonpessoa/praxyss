	;; Sum up two arguments 
;; fastcall version

	global sum

	section .text

sum:
	mov eax, ecx
	add eax, edx
	ret

