	;; A minimal running example

	global _start		; Mandatory label for entry point

	section .text		; Text (program code) section

_start:				; The entry point starts here
	mov ebx, 42		; Exit status is returned to the kenrel in ebx
	mov eax, 1		; Prepare to call syscall 1 (exit)
	int 0x80		; Perform syscall (kernel will check ebx)
