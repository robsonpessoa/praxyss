       ;; Hello x86 assembly world
       ;; Trivial

        global _start
        
        section .data
        msg db "Hello World", 0xa
        
        section .text

_start:
        mov eax, 4              ; syscall write 
        mov ebx, 1              ; file descriptor
        mov ecx, msg            ; start address
        mov edx, 12             ; how many bytes
        int 0x80                ; perform syscall


        mov ebx, 0              ; return status code
        mov eax, 1              ; syscall exit 
        int 0x80                ; perform syscall
