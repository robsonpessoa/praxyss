;; Stack method                                                                                          
;; Version 1: a crude implementation that only produces the correct                                       
;; result but leaves the stack all messed up.                                                            

global _start

section .text

_start:

        push 2                  ; Push second operand onto the stack                                             
        push 5                  ; Push first operand onto the stack                                              
        call my_func            ; Pushes return address and jump to my_func                                      

        mov ebx, eax            ; Exit with computed value                                                       
        mov eax, 1
        int 0x80

my_func:
        mov eax, [esp + 4]      ; Read first operand                                                             
        mov ebx, [esp + 8]      ; Read first operand                                                             
        sub eax, ebx            ; Sub in eax, the return register                                                
        ret
