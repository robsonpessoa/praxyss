      ;; A minimal c-like example

	global _start          ; Mandatory label for entry point           

	section .text          ; Program code segment                      

_start:                       ; The entry point starts here                            

        call main             ; push esp, then jmp main
        mov ebx, eax          ; get mains's return value from eax 

        mov eax, 1            ; Prepare for syscall exit 
        int 0x80              ; Perform syscall                                        

main:
        mov eax, 42            ; Let's store return value in, say, eax
        ret                    ; pop esp, then jmp esp

