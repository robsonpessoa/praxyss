;; Hello x86 assembly world 
;; With x86 cdecl ABI

        global _start
        global main
        global write
        
        section .data
        msg db "Hello World", 0xa
        
        section .text

_start:
        call main

        mov ebx, 0              ; return status code
        mov eax, 1              ; syscall exit
        int 0x80                ; perform syscall

main:
        push ebp                ;Prologue
        mov ebp, esp

        push 12                 ;Call write 
        push msg
        push 1
        call write

        mov esp, ebp            ;Epilogue
        pop ebp

        ret 3*8                 ;Clean stack (cdecl)

write:
        push ebp                ;Prologue
        mov ebp, esp
        add esp, 8              ;return address + ebp
        
        pop ebx                 ; file descriptor
        pop ecx                 ; start address
        pop edx                 ; how many bytes
        mov eax, 4              ; syscall write 
        int 0x80                ; perform syscall

        mov esp, ebp            ;Epilogue
        pop ebp
        ret

