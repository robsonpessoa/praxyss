;; Standard call - stdcall                                                                                         
;; Callee cleans the stack                                      
                                             
global _start

section .text

_start:

        push 2         ; Push second operand                                             
        push 5         ; Push first operand
	call my_func   ; Push return address and jump

        mov ebx, eax   ; Exit with computed value                                                       
        mov eax, 1
        int 0x80

my_func:
        push ebp       ; Save original ebp
        mov ebp, esp   ; Save original esp                                                              
        add esp, 4     ; Skip the return address     

        pop eax	   ; Read first operand                                                             
        pop ebx	   ; Read first operand                                                             
        sub eax, ebx   ; Sub in the return register
                                               
        mov esp, ebp   ; restore original esp              
        pop ebp        ; Restore original ebp

        ret 8          ; Clean the stack
