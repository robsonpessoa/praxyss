;; Stack method                                                                                          
;; Version 3: a more elegant solution with x86 base pointer                                       
                                                           

global _start

section .text

_start:

        push 2                  ; Push second operand onto the stack                                             
        push 5                  ; Push first operand onto the stack                                              
        call my_func            ; Pushes return address and jump to my_func                                      

        mov ebx, eax            ; Exit with computed value                                                       
        mov eax, 1
        int 0x80

my_func:
        push ebp                ; Save original ebp
        mov ebp, esp            ; Save original esp                                                              
        add esp, 8              ; We have to skip the return address and ebp    

        pop eax		            ; Read first operand                                                             
        pop ebx		            ; Read first operand                                                             
        sub eax, ebx            ; Sub in eax, the return register 
                                               
        mov esp, ebp            ; restore original esp              
        pop ebp                 ; Restore original ebp

        ret
