      ;; A very simple function call                                                     

	global _start          ; Mandatory label for entry point                        

	section .text          ; Program code segment                            

_start:                       ; The entry point starts here                            

        call my_func          ; push esp, then jmp my_funct 
        mov ebx, eax          ; get my_funct's return value from eax                             

        mov eax, 1            ; Prepare for syscall exit 
        int 0x80              ; Perform syscall                                        

my_func:
        mov eax, 42            ; Let's store return value in, say, eax
        ret                    ; pop esp, then jmp esp

