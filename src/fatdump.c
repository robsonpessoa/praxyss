/* fatdump.c - Output information about a FAT-formatted disk/image.
 
   Copyright (c) 2015, Monaco F. J. <monaco@usp.br>

   This file is part of Praxyss.

   Praxyss is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <stdlib.h>

#include "debug.h"

#define PROGRAM "fatdump"
#define VERSION "1.0.0"

void usage()
{
#define msg(s)   fprintf (stderr, s "\n");
  msg("");
  msg("Usage   " PROGRAM " [option] <input-file> ");
  msg("");
  msg("          <input-file>             if not given, reads from stdin");
  msg("");
  msg("          options:   --help        this help");
  msg("                     --version     software version");
  msg("");
}

int suspicious = 0;

/* Extended BIOS Partition Block (DOS 4.0) */

struct bpb_t
{
  unsigned short bytes_per_sector;    
  unsigned char sectors_per_cluster;
  unsigned short reserved_sectors;
  unsigned char number_of_fats;
  unsigned short root_entries;
  unsigned short total_sectors;
  unsigned char media_descriptor; 	 
  unsigned short sectors_per_fat;
  unsigned short sectors_per_track;
  unsigned short heads_per_cylinder;
  unsigned int hidden_sectors;
  unsigned int total_sectors_large; 
  unsigned char driver_number;
  unsigned char unused;
  unsigned char ext_boot_signature;
  unsigned int serial_number;
  char volume_label[11];
  char file_system[8]; 	
}  __attribute__((packed)) ;

#define BPB_SIZE sizeof(struct bpb_t)		/* Should be 33 or 57 or 0x3c? */


int main (int argc, char **argv)
{
  FILE *fpin;
  unsigned char b[BPB_SIZE+1];
  char *rsp;
  int rs;
  struct bpb_t bpb;
  
  fpin = stdin;
  
  if (argc > 1)
    {
      if (!strcmp (argv[1], "--help"))
	{
	  usage();
	  exit(EXIT_SUCCESS);
	}
      if (!strcmp (argv[1], "--version"))
	{
	  printf ("Version: %s %s\n", PROGRAM, VERSION);
	  exit(EXIT_SUCCESS);
	}


      if (argc > 1)
	{
	  fpin = fopen(argv[1], "r");
	  sysfatal(!fpin);
	}
      
    }
  

  rsp = fgets (b, 3+1, fpin);
  sysfatal (!rsp);

  
  /* Look for DOS MBR's jump-start  

     0xeb 0x?? 0x90 : DOS 3.0, DOS 1.1
     0xe9 0x?? 0x?? : DOS 2.x, DOS 3.1
     0x90 0xeb 0x?? : DR-DOS hard disk
     see also (*1)
  */


  if (b[0] == 0xeb)
    {
      if (b[2] == 0x90)
	{
	  printf ("Volume boot sector start code:  %2x %2x %2x", b[0],b[1],b[2]);
	  printf ("   (code entry: %2x)\n", b[1]);
	}
    }
  else if (b[0] == 0xeb)
    {
      printf ("Volume boot sector start code:  %2x %2x %2x", b[0],b[1],b[2]);
      printf ("   (code entry: %4x)\n", (b[1]<<8)+b[2]);
    }
  else if (b[0] == 0x90)
    {
      if (b[1] == 0xeb)
	{
	  printf ("Volume boot sector start code:  %2x %2x %2x", b[0],b[1],b[2]);
	  printf ("   (code entry: %2x)\n", b[2]);
	  printf ("Looks like a DR-DOS formatted disk. Not supported.\n");
	  exit (EXIT_FAILURE);
	}
    }
  else
    {
      printf ("Volume boot sector start code:  %2x %2x %2x", b[0],b[1],b[2]);
	  printf ("   (Suspicious: unkown start code)\n");
	  printf ("Doesn't look like a FAT-formatted volume. Aborted.\n");
      exit (EXIT_FAILURE);
    }
  

	   
  /* Read OEM name.

   Offset 0x3, length 8. 
   System/software used to format the disk (8 byte string padded with 0x20).
   Some DOS implementation may rely on it; we don't. */

  rsp = fgets (b, 8+1, fpin);
  sysfatal (!rsp);
  printf ("OEM name:                      '%s'\n", b);


  /* Read Extended BIOS Parameter Block.

     Offset 0xB, length 51.
  */

  rsp = fgets (b, BPB_SIZE, fpin);
  sysfatal (!rsp);
  /* bpb = (struct bpb_t *) b; */

  memcpy (&bpb, b, BPB_SIZE);
  
  /* 
   * DOS 2.0 
   */
  
  printf ("BIOS Parameter Block\n");
  printf ("  bytes per logical sector:     %u\n", bpb.bytes_per_sector);
  printf ("  logical sectors per cluster:  %u\n", bpb.sectors_per_cluster);

  printf ("  count of reserved sectors:    %u", bpb.reserved_sectors);
  if (suspicious = (bpb.reserved_sectors == 0))
    printf ("        (Suspicious: not FAT?)");
  printf("\n");



  printf ("  number of FATs:               %u", bpb.number_of_fats);
    if (suspicious = (bpb.number_of_fats == 0))
    printf ("        (Suspicious: not FAT?)");
  printf("\n");

  
  printf ("  max root directory entries:   %u", bpb.root_entries);
      if (suspicious = (bpb.number_of_fats == 0))
    printf ("        (Suspicious: not FAT12/16?)");
  printf("\n");

  printf ("  total of logical sectors  :   %u", bpb.total_sectors);

  /* If zero, correct value may be at disk offset 0x20. */
  if (suspicious = (bpb.total_sectors == 0))
    printf ("        (Suspicious: not MS-DOS 4.0 compatible?)");
  printf("\n");
      
  printf ("  media descriptor:             0x%2X\n", bpb.media_descriptor); /* Break it down */
  
  switch (bpb.media_descriptor)
    {
    case 0xf0:
      printf ("       media type               floppy\n");
      printf ("       number of sides          2\n");
      printf ("       number of tracks         80\n");
      break;
    case 0xf8:
      printf ("       media type               HD\n");
      printf ("       number of sides          2\n");
      printf ("       number of tracks         80\n");
      break;
    default:
      printf ("       media type               Unknown\n");
      printf ("       number of sides          ?\n");
      printf ("       number of tracks         ?\n");

      break;
    }


  printf ("  logical sectors per FAT:      %u", bpb.sectors_per_fat);
  
  if (suspicious = (bpb.sectors_per_fat == 0))
    printf ("        (Suspicious: not FAT12/FAT16?)");
  printf ("\n");

  
  /* 
   * DOS 3.31 additions 
   */
  
  printf ("  physical sectors per track:   %u\n", bpb.sectors_per_track);
  printf ("  number of heads:              %u\n", bpb.heads_per_cylinder);

  /* New in DOS 3.0, where it's 2 rather than 4 bytes long. */
  
  printf ("  count of hidden sectors:      %u", bpb.hidden_sectors);

  /* Enty at offset 0x13 contains 0. */
  if (bpb.total_sectors == 0)
    printf ("  (Suspicious: entry at 0x13 = 0)"); 
  printf("\n");

  /* New in DOS 3.2 where it's 2 bytes rather than 4 bytes long. */
  
  printf ("  count of all sectors:         %u\n", bpb.total_sectors_large);


  /* 
   * DOS 4.0 : Extended BPB 
   */

  /* This program does not support non MS-DOS standard disks. */

  if ( (bpb.media_descriptor == 0xf0) || (bpb.media_descriptor == 0xf8))
    {
  
      printf ("  physical driver number:       %u\n", bpb.driver_number);
      printf ("  reserved                      %u\n", bpb.unused);
      printf ("  extended boot signature  :    0x%2X\n", bpb.ext_boot_signature);
      printf ("  volume ID (serial number):    %u\n", bpb.serial_number);
      if (!suspicious)
	{
	  printf ("  partition volume label:      '%-11.11s'\n", bpb.volume_label);
	  printf ("  file system type:            '%-8.8s'\n", bpb.file_system);
	}
    }
  else
    {
      printf ("Media type %2X not fully supported.\n", bpb.media_descriptor);
    }

  /* Look for boot sector signature. */
  
  rs = fseek(fpin, 510, SEEK_SET);
  sysfatal (rs<0);

  rsp = fgets (b, 3, fpin);

  if (*(unsigned short *)b == 0xaa55)
    printf ("Boot signature found:           0x%x\n", *(unsigned short*)b);
  
  if(fpin != stdin)
    fclose (stdin);

  return EXIT_SUCCESS;
}


/* Notes

   ISO/IEC 9293 (also ECMA-107) specifies the Flexible Disk Cartrige (FDC) for
   the interchange of information between users of information processing 
   systems. This is how different devices can identify, for instance, that a
   floppy disk or a USP flash stick is FAT-formatted.

   The very first bytes of a FAT-formatted device starts with the (Extended)
   FDC descriptor, which contains (offset and length in bytes):

   Offset   Length   Purpose
   0x000    3        DOS/MBR signature (jump to code entry)
   0x003    8        OEM Name (system where the disk was formatted)
   0x00B    varies   Bios Parameter Block (BPB)

   BPB contains further information about the disk structure.


   Footnotes:

   (*1) https://en.wikipedia.org/wiki/Design_of_the_FAT_file_system#BIOS_Parameter_Block
        Wikipedia mentions 0x69 0x?? 0x?? as a valid jump-start token allowed
	by MS-DOS, PC DOS and DR-DOS on removable disks for backward compability.
	Consulted x86 instruction set reference, 0x69 is listed as IMUL (signed
	multiply), not a jump. As of now, no other reference confirming this
	mode was found.

   (*2) http://www.brokenthorn.com/Resources/OSDev5.html
        Refence states that the Media Descriptor Byte is a byte code that 
	contains information about the disk. This byte is a Bit Pattern:
	Bits 0: Sides/Heads = 0 if it is single sided, 1 if its double sided
	Bits 1: Size = 0 if it has 9 sectors per FAT, 1 if it has 8.
	Bits 2: Density = 0 if it has 80 tracks, 1 if it is 40 tracks.
	Bits 3: Type = 0 if its a fixed disk (Such as hard drive), 
	               1 if removable (Such as floppy drive)
	Bits 4 to 7 are unused, and always 1.

	Empirical experiments (mkfs.fat), however, seems not to confirm this.
	For 0xf0, 0xf8, the rule seems to work for bits 2,3 and 4 on tested
	media, but fails for bit 1. 

	if ( (bpb.media_descriptor == 0xf0) || (bpb.media_descriptor == 0xf8))
	{
	 printf ("       number of sides          %d\n", bpb.media_descriptor & 0x01 ? 2 : 1);
	 printf ("       media type               %s\n", bpb.media_descriptor & 0x08 ? "removable" : "fixed");
	 printf ("       number of tracks         %d\n", bpb.media_descriptor & 0x04 ? 40 : 80);
	 printf ("       sector/fat               %d\n", bpb.media_descriptor & 0x02 ? 8 : 9);
	}

	Moreover, Wikipedia, at
	https://en.wikipedia.org/wiki/Design_of_the_FAT_file_system#BIOS_Parameter_Block
	seems not to agree with the former hypotheses. Rather, media descriptor
	looks like quite an ad hoc attribution.

 */
