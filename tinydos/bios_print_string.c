#include "asmx86.h"

/* Print str by issuing BIOS INT 0x10. */

void  bios_print (const char *str)
{
  int i;
  
  set_eax(0x0);
  set_ah(0x0e);
  
  i = 0;
  while (str[i] != 0)
    {
      set_al(str[i]);
      bios_int(0x10);
      i++;
    }

}
