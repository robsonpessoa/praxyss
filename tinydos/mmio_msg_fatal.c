/* mmio_msg_fatal.c - Fata error; abort. 
   
   Outputs a message and halts.
*/

#include "asmx86.h"

void NAKED mmio_msg_fatal ()
{

  _(movw $0xb800, %ax);
  _(movw %ax, %es);
  
  _(movw $0x4f46, %ax);	   /* 'F', 0x4f */
  _(movw %ax, %es:0x0000);
  _(movw $0x4f61, %ax);	   /* 'a', 0x4f */
  _(movw %ax, %es:0x0002);
  _(movw $0x4f74, %ax);	   /* 't', 0x4f */
  _(movw %ax, %es:0x0004);
  _(movw $0x4f61, %ax);	   /* 'a', 0x4f */
  _(movw %ax, %es:0x0006);
  _(movw $0x4f6c, %ax);	   /* 'l', 0x4f */
  _(movw %ax, %es:0x0008);

  _(cli);
  _(hlt);
  
}
