#include "asmx86.h"

/* Print str on screen using memmory-mapped video memory.
   Argument mode allow to choose appearance. */

void mmio_print_string (const char *str, char mode)
{
  short *v = (short *) seg_to_lin(0xb800, 0x0000);
  int i;
  
  i = 0;
  while (str[i])
    {
      v[i] = (mode << 8) + str[i];
      ++i;
    }
}
