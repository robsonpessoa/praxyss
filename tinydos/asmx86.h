#ifndef X86ASM_H
#define X86ASM_H

/* Short aliases. */

#include "../config.h"

#define _(...) __asm__(#__VA_ARGS__ "\n\t")

#if NAKED_ATTRIBUTE == 1
#define NAKED __attribute__ ((naked)) 
#else
#define NAKED
#endif

#define _easm_ __asm__ volatile 

/* Convert linear to segmented memory address. */

#define seg_to_lin(segment,offset) ((segment << 4) + offset)

/* Write into and read from 8, 16 and 32bit 
   general-purpose register variables. */

#define set_l(reg,  val) _easm_ ("movb %0, %%"  #reg "l"::"g"(val): #reg "l")
#define set_h(reg,  val) _easm_ ("movb %0, %%"  #reg "h"::"g"(val): #reg "h")
#define set_x(reg,  val) _easm_ ("movw %0, %%"  #reg "x"::"g"(val): #reg "x")
#define set_ex(reg, val) _easm_ ("movl %0, %%e" #reg "x"::"g"(val): #reg "x")

#define get_l(reg, des)   _easm_ ("movb %%"  #reg "l, %0":"=m"(des):)
#define get_h(reg, des)   _easm_ ("movb %%"  #reg "h, %0":"=m"(des):)
#define get_x(reg, des)   _easm_ ("movw %%"  #reg "x, %0":"=m"(des):)
#define get_ex(reg, des)  _easm_ ("movl %%e" #reg "x, %0":"=m"(des):)

#define set_al(val)  set_l(a, val)
#define set_ah(val)  set_h(a, val)
#define set_ax(val)  set_x(a, val)
#define set_eax(val) set_ex(a, val) 
#define get_al(ptr)  set_l(a, *ptr)
#define get_ah(ptr)  set_h(a, *ptr)
#define get_ax(ptr)  set_x(a, *ptr)
#define get_eax(ptr) set_ex(a, *ptr)

#define set_bl(val)  set_l(b, val)
#define set_bh(val)  set_h(b, val)
#define set_bx(val)  set_x(b, val)
#define set_ebx(val) set_ex(b, val)
#define get_bl(ptr)  set_l(b, *ptr)
#define get_bh(ptr)  set_h(b, *ptr)
#define get_bx(ptr)  set_x(b, *ptr)
#define get_ebx(ptr) set_ex(b, *ptr)

#define set_cl(val)  set_l(c, val)
#define set_ch(val)  set_h(c, val)
#define set_cx(val)  set_x(c, val)
#define set_ecx(val) set_ex(c, val)
#define get_cl(ptr)  set_l(c, *ptr)
#define get_ch(ptr)  set_h(c, *ptr)
#define get_cx(ptr)  set_x(c, *ptr)
#define get_ecx(ptr) set_ex(c, *ptr)

#define set_dl(val)  set_l(d, val)
#define set_dh(val)  set_h(d, val)
#define set_dx(val)  set_x(d, val)
#define set_edx(val) set_ex(d, val)
#define get_dl(ptr)  set_l(d, *ptr)
#define get_dh(ptr)  set_h(d, *ptr)
#define get_dx(ptr)  set_x(d, *ptr)
#define get_edx(ptr) set_ex(d, *ptr)

  
#define zero_l(reg)   _easm_("xor %%" #reg  "l, %%" #reg "l" ::: #reg "l")
#define zero_h(reg)   _easm_("xor %%" #reg  "h, %%" #reg "h" ::: #reg "h")
#define zero_x(reg)   _easm_("xor %%" #reg  "x, %%" #reg "x" ::: #reg "x")
#define zero_ex(reg)  _easm_("xor %%e" #reg "l, %%e" #reg "x"::: #reg "x")

#define zero_al()  zero_l(a)
#define zero_ah()  zero_h(a)
#define zero_ax()  zero_x(a)
#define zero_eax() zero_ex(a)

#define zero_bl()  zero_l(b)
#define zero_bh()  zero_h(b)
#define zero_bx()  zero_x(b)
#define zero_ebx() zero_ex(b)

#define zero_cl()  zero_l(c)
#define zero_ch()  zero_h(c)
#define zero_cx()  zero_x(c)
#define zero_ecx() zero_ex(c)

#define zero_dl()  zero_l(d)
#define zero_dh()  zero_h(d)
#define zero_dx()  zero_x(d)
#define zero_edx() zero_ex(d)


/* Set offse & segment registers. */

#define set_offset(reg, val) _easm_ ("mov %0, %%" #reg ::"g"(val): #reg)

#define set_ip(val) set_offset(ip, val)  /* This is illegal. */
#define set_si(val) set_offset(si, val)
#define set_sp(val) set_offset(sp, val)
#define set_bp(val) set_offset(bp, val)
#define set_di(val) set_offset(di, val)

#define set_eip(val) set_offset(eip, val) /* This is illegal. */
#define set_esi(val) set_offset(esi, val)
#define set_esp(val) set_offset(esp, val)
#define set_ebp(val) set_offset(ebp, val)
#define set_edi(val) set_offset(edi, val)

#define set_cs(val) _easm_("pushl %%eax; movl %0, %%eax; movl %%eax, %%cs; popl %%eax"::"g"(val):"eax")
#define set_ds(val) _easm_("pushl %%eax; movl %0, %%eax; movl %%eax, %%ds; popl %%eax"::"g"(val):"eax")
#define set_ss(val) _easm_("pushl %%eax; movl %0, %%eax; movl %%eax, %%ss; popl %%eax"::"g"(val):"eax")
#define set_es(val) _easm_("pushl %%eax; movl %0, %%eax; movl %%eax, %%es; popl %%eax"::"g"(val):"eax")


/* Read flag registers

   Flagas are temporarily return in ah = EFLAGS(SF:ZF:0:AF:0:PF:1:CF) 
   Register eax is preserved and restored using the stack. */

#define read_eflags_lsb(ptr) _easm_("pushl %%eax; lahf; movw %%ax, %0; popl %%eax":"=m"(*ptr)::"eax")
  
#define get_cf(flags) (flags & 0x0001)
#define get_pf(flags) (flags & 0x0004)
#define get_af(flags) (flags & 0x0010)
#define get_zf(flags) (flags & 0x0040)
#define get_sf(flags) (flags & 0x0080)

/* Register operations */

#define get_mem(src, dest)  _easm_("mov (%%" #src "), %%" #dest ::: #src, #dest)

#define sum_reg(reg1, reg2) _easm_("add %%" #reg1  ", %%" #reg2 ::: #reg1, #reg2)

/* Function main (called by _start). */

int main();

/* Low-level utility functions. */

#define bios_int(interrupt) _easm_("int $" #interrupt:::)

#define halt() _easm_("cli \n\t hlt":::)

/* 
 *  Lib asmx86 functions (must be linked). 
 */

/* Print str using BIOS INT 0x10 interrupt. */

void  bios_print_string (const char *str);

/* Print 'str' through Video Memory, using color 'mode'*/

/* (d: dark, l: light. */

#define VGA_BLACK    0x0	
#define VGA_dBLUE    0x1
#define VGA_dGREEN   0x2
#define VGA_dCYAN    0x3
#define VGA_dRED     0x4
#define VGA_dMAGENTA 0x5
#define VGA_BROWN    0x6
#define VGA_lGRAY    0x7
#define VGA_dGRAY    0x8
#define VGA_lBLUE    0x9
#define VGA_lGREEN   0xa
#define VGA_lCYAN    0xb
#define VGA_lMAGENTA 0xc
#define VGA_YEALLOW  0xd
#define VGA_WHITE    0xe

#define VGA_FGBG(fg, bg) (bg<<4 + fg)

void mmio_print_string (const char *str, char mode);

void mmio_msg_ok (void);
void mmio_msg_no (void);
void mmio_msg_fatal (void);
void mmio_dump (const char *, int); /* Deprecate. */
  
#endif	/* X86ASM_H */

