/* mmio_msg_ok.c - Memory efficient message: Ok */

#include "asmx86.h"

void NAKED mmio_msg_ok ()
{
  _(pushl %eax);
  _(pushl %ecx);

  /* _(xor %eax, %eax); */
  /* _(movl %eax, %ecx);   */
  
  _(movl $0xb8000, %ecx);

  _(movl $0x304f, %eax);	/* 'O', 0x30 */
  _(movl %eax, (%ecx));
  _(movl $0x306b, %eax);	/* 'k', 0x30 */
  _(movl %eax, 2(%ecx));

  _(popl %ecx);
  _(popl %eax);

}
