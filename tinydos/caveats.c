
void __attribute__((naked)) _foo (void)
{

  register unsigned int var_eax __asm__("eax");
  register unsigned int var_ebx __asm__("ebx");

  var_eax = 1; 

  var_ebx = var_eax + 1;
  
  __asm__("hlt");
  
}
