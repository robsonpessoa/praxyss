/*    stage1-01.c - Stage 2 is in the next cluster.

      We're not using FAT for now.
*/

#include "asmx86.h"

#define STAGE2_ADDRESS 0x7e00

/* In order not to clutter the source code with error checks, we don't
   perform all the due verifications. Later on we shall address this. */

int main(void)           
{
  char flags;			              /* Processor flags */
  //void (*stage2)() = (void *) STAGE2_ADDRESS;

  /* Reset floppy A: INT 0x13                                        */ 

  do
    {
      set_ah (0);		        /* Set reset operation         */
      set_dl (0);		        /* Set floppy (drive 0)        */
      bios_int (0x13);		        /* Call BIOS                   */
      read_eflags_lsb(&flags);
    }
  while( get_cf(flags) );


  /* Program stage2 is right at the next sector.
     We'll load id into address (BIOS_load_address + stage1_size) 
     using BIOS INT 0x13 function: 

     AH	     Function 02h
     AL	     Sectors (length)
     CH	     Cylinder
     CL	     Sector
     DH	     Head
     DL	     Drive
     ES:BX   Address (target)
*/

  //set_ecx(0x0);
  
  set_ah(0x2);		                 /* Function read.  */
  set_al(0x1);				 /* Read 1 sector.  */
  set_ch(0x0);	         		 /* Cylinder 0      */
  set_cl(0x2);				 /* Sector 2        */
  set_dh(0x0);				 /* Head 0          */
  set_dl(0x0);				 /* Drive 0         */
  
  set_es(0x0000);		         /* Segement.       */
  set_bx(0x7e00);  	                 /* Offset.         */

  bios_int(0x13);
  
  read_eflags_lsb(&flags);
  if (!get_cf(flags))
    mmio_print_string ("Read error", 0x30);

  /* If everything succeeds ok, we should not see this string, as it will be
     immediately overwritten by stage2's own output. */

  mmio_print_string ("Load stage 2...", 0x0f); 

  /* The statement stage2() is equivalent to a call to 0x000:0x7e00 */

  //stage2();

  _(jmp 0x7e00);
  
  mmio_print_string ("Error: stage 2 not loaded", 0x4f);

  _(cli);
  _(hlt);

  return 0;
}


/*  Notes.

    

*/


#if 0

do				
    {
      set_ah (0);		/* Set reset operation         */
      set_dl (0);		/* Set floppy (drive 0)        */
      bios_int (0x13);		/* Call BIOS                   */
      read_flags (&a);		/* Read return status (flags)  */
    }
  while (get_cf(a));		/* On error, CF is set; retry. */

#endif
