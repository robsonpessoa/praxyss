#include "asmx86.h"

/* This is the start (upper bound) of our stack (defined in the linker script).*/

extern void __END_STACK__;	

/* Since stage1 is a flat binary file and it's executed diretly by BIOS,
   our entry point is the very first byte (loaded at 0x7c00). */

_(start:);
/* Ensure segement registers are coorectly set after program is loaded. 
     Except by a few exceptions, BIOS does not initialize most registers. */ 

_(xorw %ax, %ax);
_(movw %ax, %ds);		/* Set data segement ds. */
_(movw %ax, %es);		/* Set extra segment es. */
_(movw %ax, %fs);		/* Set extra segment fs. */
_(movw %ax, %gs);		/* Set extra segment gs. */

_(jmp init);			/* Set CS:SI pair.*/
_(init:);			


_(movl $__END_STACK__, %esp);	/* Set SS:SP pair.*/

/* Call main(), as in a regular C program. */

_(jmp main);



/* Notes.

   This startup code must be the first bytes in the main program's binary file.
   It's important because, differently from a regular elf file, we can't specify
   an entry point in a flat binary executable. Upon booting, it will execute from
   begining to end as it is.

*/
