/* mmio_msg_ok.c - Memory efficient message: Ok */

#include "asmx86.h"
#define mode 0x60

void mmio_dump (const char *p, int len)
{
  short *v = (short *) seg_to_lin(0xb800, 0x0000);
  int i;
  
  for (i = 0; i<len; i++)
    {
      v[i] = (mode << 8) + p[i];
    }

}
