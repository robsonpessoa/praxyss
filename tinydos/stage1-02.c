/* stage1-02.c - Stage 2 is a file in a FAT12 floppy.

   Parsing a FAT12 filesystem in search of Stage-2 boot file is relatively less
   trivial than merely loading a predefined sector and may require 
   considerably more instructions. In order not to run out of memory with our 
   limited 512-byte Stage-1 program, we take a more parsimonious approach 
   when it comes to resorting to our helper macros; instead, we fall back to
   manually optimizing some inline assembly for reducing code size.

   To that end, we unceremoniously and serenely rely on several GCC extensions.

*/

#include "asmx86.h"

#define STAGE1_ADDR 0x7c00	/* BIOS load stage1 0x000:0x7c00 (x86 specs).*/
#define FAT_ADDR    0x7e00	/* Let's load FAT data at 0x0000:0x7c00+512.*/
#define STAGE2_ADDR 0x20000     /* Let's load stage2 at 0x2000:0x000    */

const char stage2_name[] = "STAGE2  BIN";

/* Auxiliary function at the bottom of this file. */

extern void fatal();		

/* Return 1 if 11-byte long str1 and str2 are equal; return 0 otherwise.
   Writing this function in C is the only luxury we allow ourselves to afford, 
   since string manipulation is possibly the more cumbersome part of the code.*/

#define FILE_NAME_LENGTH 11	/* Fixed file name length; FAT12 specs.  */

/* Ok, lets do it. */

int main(void)           
{
  int count;

  /* It's always advisable initialize the IO device. Hardware is physically
     susceptible to faults for a variety of reasons... and we never know. 
     Let's thus reset floppy 0 just for the case. */

  set_ebx(5);			       /* Allow up to 5 retries.   */
  {
    __label__ retry_reset;
    
    retry_reset:
      set_ah (0);		               /* Set reset operation      */
      set_dl (0);		               /* Set floppy (drive 0)     */
      bios_int (0x13);		               /* Call BIOS                */
      _(dec %ebx);                             /* On error, retry a few    */
      _(jz fatal);			       /* times; if still faulty,  */
      __asm__ goto ("jc %l0"::::retry_reset);  /* than fail-stop.          */
    }
  
  /* Now our roadmap is:

     a) Having the Directory Root loaded into RAM, we shall go through it in 
        search of a 32-bit entry whose file name is the name of our stage2 boot 
	program.
     b) Having found it, we shall check the last bytes of this entry for to know 
        the position of it's corresponding frist 11-bit entry in FAT area.
     c) Then, having the FAT load into ram, we shall read the located entry and
        start to follow the breadcrumbs coded as a linked list FAT12 map; for each
	FAT block entry, we shall load the corresponding cluster (in data area)
	int RAM.

     According to this algorithm, we might load the Directory Root, locate the
     stage2's entry, then load the FAT. Alternative, a more parsimonious
     approach to save us some precious bytes of our 512-byte limit, we may take
     the opportunity and load both the FAT and the Directory Root at once,
     issuing only one single BIOS interrupt. 
     
  
     BIOS INT 0x13 function 02h: 

     AH	     Function 02h              This is the function 'read'
     AL	     Sectors (length)          The number of sectors to be read
     CH	     Cylinder                  Starting cylinder in CHS geometry
     CL	     Sector                    Starting physical sector in CHS geometry
     DH	     Head		       Head number in CHS geometry  
     DL	     Drive                     Driver number (e.g. floppy is 0)
     ES:BX   Address (target)	       Where in RAM data should be written.
 


   Here we assume we are booting from a FAT12-format disk with the 
   following standard logical geometry. We're using macros to be resolved
   at compile time in order to reduce binary memory footprint. If there's
   room left, some fields might be actually read from the disk BPB. 

   One may use the auxiliary program in ../src to check

     $ fatdump floppy-02.img

   These are typical values for a FAT-12 formatted disk.

  */
  

#define BYTES_PER_SECTOR    512
#define SECTORS_PER_CLUSTER   1
#define RESERVED_SECTORS      1
#define SECTORS_PER_FAT       9
#define NUMBER_OF_FATS        2
#define DIR_ENTRIES         224
#define DIR_ENTRY_LENGTH     32
#define SECTORS_PER_TRACK    18
#define NUMBER_OF_HEADS       2
#define DRIVE_NUMBER          0	/* BIOS has supposedly left it in %dh. */


  /* Now we need to do some calculations to know which and how many disk's
     sectors to read in to RAM.  We don't need to worry about code length
     because our calculations are performed at compilation time by the
     C preprocessor (that's why we're using macros a lot).

     The first FAT starts at FAT_OFFSET from the start of the disk and
     we may have NUMBER_OF_FAT of them; each FAT is SECTORS_PER_FAT long.
     Then, right after the FAT(s), at DIR_OFFSET, Directory follows; it
     is DIR_LENGTH sectors long. */

#define FAT_OFFSET       (RESERVED_SECTORS)
#define FAT_LENGTH       (SECTORS_PER_FAT)

#define DIR_OFFSET       (RESERVED_SECTORS + (SECTORS_PER_FAT * NUMBER_OF_FATS))
#define DIR_LENGTH       ((DIR_ENTRIES * DIR_ENTRY_LENGTH) / BYTES_PER_SECTOR)

  /* This is where to start and how many sectors to read to have (all) the 
     FAT(s), plus the Directory into RAM. */
  
#define OFFSET      (FAT_OFFSET)
#define LENGTH     ((FAT_LENGTH * NUMBER_OF_FATS) + DIR_LENGTH)

  /* The offset we calculated is a linear count, starting from 0. At this
     stage, we are using BIOS interrupts which only understand CHS geometry.
     We therefore need to convert Linear Block Addressing (LBA) into 
     Cylinder-Head-Sector (CHS) geometry. */
  
  
#define LBA           (OFFSET)
  
#define CYLINDER      (LBA / (SECTORS_PER_TRACK  * NUMBER_OF_HEADS))
#define HEAD         ((LBA /  SECTORS_PER_TRACK) % NUMBER_OF_HEADS)
#define SECTOR       ((LBA %  SECTORS_PER_TRACK) + 1)
    
  /* Load Root Directory into RAM by using BIOS int 13h.
     We shall go through it in search of an entry with Stage2 file name.

     Since, further on, we'll need to load the FAT area, we take this
     opportunity to do it as well, all in a single BIOS call. This should
     save us some precious bytes in our binary code. */

  set_ah(0x2);		                 /* Function read.         */
  set_al(LENGTH);			 /* How many sectors.  */
  set_ch(CYLINDER);	         	 /* Cylinder       */
  set_cl(SECTOR);			 /* Sector         */
  set_dh(HEAD);			         /* Head           */
  set_dl(0x0);				 /* Drive 0         */

  set_es(0x0000);		         /* Segement.       */
  set_bx(0x7e00);	                 /* Offset.         */

  bios_int (0x13);		                 /* Call BIOS. */
  _(jc fatal);


  /* At this point we can access the FAT map starting at FAT_RAM_OFFSET, 
     and the Directory information starting at DIR_RAM_OFFSET. */
  
#define FAT_RAM_OFFSET   (FAT_ADDR)
#define DIR_RAM_OFFSET   (FAT_ADDR + (FAT_LENGTH * NUMBER_OF_FATS * 512))  


  /* Let's search for a file named STAGE2_NAME.
     On failure, fail stop. */
  
#define FAT12_ENTRY_LENGTH 32


  /* What we need is something like 

     count = DIR_RAM_OFFSET;
     while ((!is_filename ((char *) (count), stage2_name)) && 
            (count < (DIR_RAM_OFFSET + DIR_ENTRIES*FAT12_ENTRY_LENGTH)))
        count += FAT12_ENTRY_LENGTH;

     where is_filename is a function comparing two strings.  No matter how
     tempting this C code is, we can't afford its corresponding 
     compiler-generated assembly. Instead, we'll resort to 'repz cmpsb'
     assembly construct (see x86 instruction manual).

     Code repz cmpsb compare each byte of string A (in %esi) against the 
     corresponding byte of string B (in %edi), decrementing a counter (in %cx)

     CX           string length.  
     DS:SI        string A
     ES:DI        string B
    
     until either the counter reach zero (strings are equal) or a mismatch
     is found (strings differ).

     Our algorithm goes through all directory entries comparing it's first 11
     bytes (file name) until either a match is found or the directory end
     is reached.

*/

  set_cx(FILE_NAME_LENGTH);	/* String length. */

  set_eax(DIR_ENTRIES);		/* Last entry.    */
  set_ebx(0);			/* Entry counter. */

  {
    __label__ search_stage2, found_stage2;

    _(cld);			/* Compare left-to-right.  */

  search_stage2:
    set_esi(stage2_name);       /* Predefined stage2 name. */  
    set_edi(DIR_RAM_OFFSET);    /* Directory entry offset. */
    sum_reg(ebx, edi);          /* Next directory entry.   */
    
    _(repz cmpsb);
    __asm__ goto ("jz %l0"::::found_stage2);

    _(addl $0x20, %ebx);
    _(cmp %ebx, %eax);
    _(je fatal);
    
    __asm__ goto ("jmp %l0"::::search_stage2);
    
  found_stage2:
    _easm_ ("movl %%ebx, %0":"=m"(count)::); 

#if 0				                  /* Set 1 to debug */
    _easm_ ("movl %0, %%edx"::"m"(count):"edx");
    mmio_msg_ok();
    halt();
#endif
    
  }

  
  /* This is where the floppy data area begins. */
  
#define DATA_RAM_OFFSET (DIR_RAM_OFFSET + DIR_ENTRIES * DIR_ENTRY_LENGTH)


  /* The linear address of the first sector of stage2 is

       lba  = DATA_RAM_OFFSET + (count + 26)

     The translation fom lba to to CHS geometry is, as before, given by the 
     conversion expressions bellow:

       cylinder =  (lba / (SECTORS_PER_TRACK  * NUMBER_OF_HEADS));
       head =     ((lba /  SECTORS_PER_TRACK) % NUMBER_OF_HEADS);
       sector=    ((lba %  SECTORS_PER_TRACK) + 1);

     These values should be fed into the corresponding general-purpose registers
     specified by BIOS INT 13h. The assembly code generated by the compiler out
     of the C code above, however, is overwhelmingly large, as it involves using
     memory operations and redundant arithmetic procedures. Sadly, we just
     can't afford it.  Rather, in the following passages, we once more resort to
     hand-optimized (even if perhaps not so intuitive) assembly.

     It goes like this: we know that, at this point, the file corresponding to 
     stage2 is the ebx-th 32-byte entry in the file system directory. 
     The first cluster to read is given by it's 26th byte. 
     Let's load this value (lba) into %eax. */

  set_eax (DATA_RAM_OFFSET+26);
  sum_reg (ebx, eax);

  /* Let's perform a byte-length integer division using instruction div. */

  set_bl (SECTORS_PER_TRACK);	
  _(div %bl);                  

  /* Now, as a desired effect, we have (integer division) 

     %al = lba / SECTORS_PER_TRACK 
     %ah = lba % SECTORS_PER_TRACK                       */

  _(add $1, %ah);
  _(movb %ah, %cl);		/* and we can load the sector. */

  /* As for al, we do */
  
  _(and 0xff, %eax);		/* and now %eax = %al */

  /* Again, we perform an integer-division with div */

  set_bl (NUMBER_OF_HEADS);	
  _(div %bl);
  
  /* and, as result, we how have 

     %al = (lba / SECTORS_PER_TRACK) / NUMBER_OF_HEADS
     %ah = (lba / SECTORS_PER_TRACK) % NUMBER_OF_HEADS
  */

  _(movb %al, %ch);		/* This is the cylinder. */
  _(movb %ah, %dh);		/* This is the head. */
  
  /* All the rest is constant for our read. */

  set_ah(0x2);		                 /* Function read.         */
  set_al(SECTORS_PER_CLUSTER);		 /* How many sectors.  */
  set_dl(0x0);				 /* Drive 0         */
  set_es(0x2000);		         /* Segement.       */
  set_bx(0x0000);	                 /* Offset.         */
  bios_int (0x13);		         /* Call BIOS. */
  _(jc fatal);

  /* Ok, we've read the first sector of stage2 into RAM.
     Now it's your job to complete this code so that the program read the
     remaining sectors. To that end, the algorithm should check the
     corresponding 12-bit FAT12 entry to know if there are further clusters
     and what are their location in data area as we have just done.

     You should probably convert the previous source code passage into a
     routine you can repeatedly call for each cluster. Parsimony and 
     asm optimization are required. If the code does not fit the 512-byte
     limit imposed by BIOS, you may need to further optimize other parts
     of the program. Alternatively, you may opt for adding an extra stage
     to your boot procedure by making stage1 span through more than one 
     reserved sector, and then use the first part of the code to load 
     the remaining sectors (naturally, your storage volume should be 
     formatted accordingly). 

 */
  
  return 0;
}

void NAKED fatal()
{
  mmio_msg_fatal();
}




