/*    Hello World from real-mode memmory mapped video.

      Same as mmio-03.c, printing a string.
*/


#include "asmx86.h"

#define GREETINGS "Stage 2 successfully loaded!                                                    "

/* This will be our entry point */

int main(void)           
{
  mmio_print_string (GREETINGS, 0x20);

  halt();

  return 0;
}


/*  Notes.


*/




