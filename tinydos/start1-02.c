#include "asmx86.h"

extern void __END_STACK__;	/* Defined by the linker script. */

/* Ensure segement registers are ok after program is loaded  */

_(xorw %ax, %ax);
_(movw %ax, %ds);
_(movw %ax, %es);
_(movw %ax, %fs);
_(movw %ax, %gs);

/* Set CS:SI os 0x000:7c00*/
_(jmp init);
_(init:);

/* Initialize stack pointer */
_(movl $__END_STACK__, %esp);

_(call _start);

void _start()    /* Starts call main. */
{
  main();
}


/* Notes.

   This startup code must be the first bytes in the main program's binary file.
   It's important because, differently from a regular elf file, we can't specify
   an entry point in a flat binary executable. Upon booting, it will execute from
   begining to end as it is.

*/
