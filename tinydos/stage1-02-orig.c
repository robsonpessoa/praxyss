/*    stage1-02.c - Stage 2 is a file in a FAT12 floppy.

      Reading FAT12 takes requires more instructions than reading a raw sector.
      In order not to run out of memory with our limited 512 byte boot sector,
      we go without our helper macros here and optmize assembly manually.
*/

#include "asmx86.h"

#define STAGE1_ADDR 0x7c00
#define STAGE2_ADDR 0x7e00


char str_match (char *str1, char *str2, int length)
{
  int i=0;
  while (i<length)
    {
      if (str1[i] != str2[i])
      return 0;
      i++;
    }
  return 1;
}


/* In order not to clutter the source code with error checks, we don't
   perform all the due verifications. Later on we shall address this. */

int main(void)           
{
  int volatile count;
  
  /* Reset floppy A: INT 0x13 (optmized).*/
        
  _(reset:);
  set_ah (0);		        /* Set reset operation         */
  set_dl (0);		        /* Set floppy (drive 0)        */
  bios_int (0x13);		/* Call BIOS                   */
  _(jc reset);			/* On error, retry.            */

  /* Boot sector is loaded into 0x7c00, therefore BPB is there.
     We can parse it to get the offset of FAT12's directory entry. */
#if 0
  bpb =  (struct bpb_t *) ((char *) (0x7c00 + 11));
  root_offset = bpb->reserved_sectors + (bpb->number_of_fats * bpb->sectors_per_fat) ;

  root_length = (32 * bpb->root_entries) / bpb->bytes_per_sector; 
#endif
  /* Load root entry into 7c00+0x200 = 7e00. */

  /* BIOS INT 0x13 function 02h: 

     AH	     Function 02h
     AL	     Sectors (length)
     CH	     Cylinder
     CL	     Sector
     DH	     Head
     DL	     Drive
     ES:BX   Address (target)
*/

#if 0
  sector 	= 	(root_offset % bpb->sectors_per_track) + 1;
  cylinder   	= 	(root_offset / bpb->sectors_per_track) % NUMBER_OF_HEADS;
  cylinder 	= 	 root_offset / (bpb->sectors_per_track * NUMBER_OF_HEADS);
#endif

  /* FAT12 Defaults (disk must comply). */

#define BYTES_PER_SECTOR 512
#define RESERVED_SECTORS   1
#define SECTORS_PER_FAT    9
#define NUMBER_OF_FATS     2
#define DIR_ENTRIES      224
#define DIR_ENTRY_LENGTH  32
#define SECTORS_PER_TRACK 18
#define NUMBER_OF_HEADS    2
#define DRIVE_NUMBER       0	/* BIOS has supposedly left in %dh. */

  /* Offset in sectors. */
  
#define DIR_OFFSET       RESERVED_SECTORS + (SECTORS_PER_FAT * NUMBER_OF_FATS) 
#define DIR_LENGTH       (DIR_ENTRIES * DIR_ENTRY_LENGTH) / BYTES_PER_SECTOR

  /* Convert linear offset to CHS gemoetry. */
  
#define LBA              DIR_OFFSET

#if 0
  absolute track 	= 	 LBA / (sectors per track * number of heads)
  absolute head   	= 	(LBA / sectors per track) % number of heads
  absolute sector 	= 	(LBA % sectors per track) + 1
#endif
    
#define DIR_CYLINDER      LBA / (SECTORS_PER_TRACK  * NUMBER_OF_HEADS)
#define DIR_HEAD         (LBA /  SECTORS_PER_TRACK) % NUMBER_OF_HEADS
#define DIR_SECTOR       (LBA %  SECTORS_PER_TRACK) + 1
  
  set_ah(0x2);		                 /* Function read.         */
  set_al(DIR_LENGTH);			 /* Read DIR_LENG sectors.  */
  set_ch(DIR_CYLINDER);	         	 /* Cylinder 0      */
  set_cl(DIR_SECTOR);			 /* Sector 2        */
  set_dh(DIR_HEAD);			 /* Head 0          */
  set_dl(0x0);				 /* Drive 0         */
  
  set_es(0x0000);		         /* Segement.       */
  set_bx(STAGE2_ADDR);	                 /* Offset.         */

  _(int $0x13);		                 /* Call BIOS. */
  
  _(jc fatal);

#define DIR_RAM_OFFSET        (STAGE2_ADDR)

  
#if 0
  count = 0;
  while ((!str_match ((char *) (DIR_RAM_OFFSET+count*32), "STAGE2  BIN", 11)) && (count < DIR_ENTRIES))
    count++;

  if (count < DIR_ENTRIES)
    mmio_msg_ok();
#endif

  
  //*(char *) 0x7e00 = "ABCD";

  _(movl $65, %eax);
  _(movl %eax, %es:0x7e00);
  
  mmio_dump ((char *) 0x7e00, 512);

  //__asm__("movl %0, %%eax"::"m"(count));

  
  _(jmp .);

  _(cli);
  _(hlt);

  _(fatal:);
  mmio_msg_fatal();
  
  return 0;
}


/*  Notes.

    

*/

