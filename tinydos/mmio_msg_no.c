/* mmio_msg_no.c - Memory efficient message: No */

#include "asmx86.h"

//void NAKED mmio_msg_no ()
void NAKED mmio_msg_no ()
{
  _(pushl %eax);
  _(pushl %ecx);

  _(movl $0xb8000, %ecx);

  _(movl $0x4f4e, %eax);	/* 'N', 0x60 */
  _(movl %eax, (%ecx));
  _(movl $0x4f6f, %eax);	/* 'o', 0x60 */
  _(movl %eax, 2(%ecx));

  _(popl %ecx);
  _(popl %eax);
}
