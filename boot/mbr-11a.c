/*    Hello World in C with inline assembly.

      Like mbr-11b.c, but fixed.
*/

/* Assembly header. */

__asm__("begin:");
__asm__(".code16");         

/* The string. */

const char here[] __attribute__((section(".text#"))) = "Hello World!\r\n";

/* This will be our entry point */

void __attribute__ ((naked)) start()           
{
  register volatile int eax __asm__  ("eax");                                      
  register volatile int ebx __asm__  ("ebx");                                      

  eax = 0x0;                    
                                                                                   
  eax = 0x0e00;                 /* Load 0xe into ah. */                            
                                                                                   
  ebx = 0x0;                    /* Offest to the string. */                        
                                                                                   
  do                            
    {                                                                              
      eax &= 0xffffff00;        /*     prepare for the next line   */              
      eax |= (char) here[ebx];  /*     mov al, BYTE [here + bx]    */              
      __asm__("int     $0x10"); /*     int 0x10                    */              
      ebx++;                    /*     add bx, 0x1                 */              
                                                                                   
    }                                                                              
  while ( (eax & 0x000000ff)   != 0x0);      /*  while(ah != 0x0)  */    

  while(1);

}

/* Assembly footer. */

__asm__(". = begin + 510");                
__asm__(".byte 0x55, 0xaa");               



/*  Notes.

    We fixed mbr-11b.c by including a label 'begin' before enyting else and 
    then used it instead of 'start' and the end to calculate the file size. 
 */

