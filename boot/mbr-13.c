/*   Hello World, C only.

     Like mbr-12.c, but using linker script. */


#include "mbr-13.h"

/* The string. */

const char string[]  = "Hello World!\r\n";

/* This will be our entry point */

void NAKED _start()           
{
  int i;
  r32_t eax SET_EAX;

  eax = 0x0;                    
  set_h(eax,0x0e);
  
  i = 0;                   
  do                            
    {                                                                              
      set_l(eax, string[i] );
      bios_int(0x10);
      i++;                    
    }                                                                              
  while ( get_l(eax) != 0x0);   
  
  while(1);

}


/*  Notes.

    This program is a slightly improved version of mbr-12.c which relies on
    the linker script to unclutter source relatively to the former example.
    Script link-13.ld is passed to the liker through option (Makefile):

          -Wl,-T link-13.ld

    Notice the absence of MBR_INIT and MBR_END block macros in mbr-13.c.
    Those could be omitted because we told the linker to start byte count
    at position 0x7c00, and because we left to the linker the task of
    adding the boot signature.

    Observe also the absence of the attribute in the string definition.
    This was made possible by telling the linker in which order we want the
    code segments to appear in the elf file: first .text, than .rodata.

    Notice also that gcc invocation (Makefile) has the options 

         -Wl,--build-id=none 

    which prevents ld from generating an informational section 
    .note.gnu.build-id>, and the option

	 -Wl,--orphan-handling=discard -Wl,
    
     which tells the linker to not generate any section which is not 
     explicitly specified in the linker script.
	 
 */

