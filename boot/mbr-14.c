/*    Like mbr-13.c, with a different linker script. */


#include "mbr-14.h"

/* The string. */

const char string[]  = "Hello World!\r\n";

/* This will be our entry point */

void NAKED _start()           
{
  int i;
  r32_t eax SET_EAX;

  eax = 0x0;                    
  set_h(eax,0x0e);
  
  i = 0;                   
  do                            
    {                                                                              
      set_l(eax, string[i] );
      bios_int(0x10);
      i++;                    
    }                                                                              
  while ( get_l(eax) != 0x0);   
  
  while(1);

}


/*  Notes.

    This program is identical to mbr-13.c. It's provided only to illustrate 
    the possibility of instructing the linker to generate a flat binary
    file instead of an elf binary file

    See link-14.ld and compare with link-13.ld.

    This allow us to eliminate the objcopy in the make script.

    See Makefile.
	 
 */

