/*    Wello World with inline assembly hidden.

      Like mbr-11b.c, but with the blue pill. */


#include "mbr-12.h"

MBR_INIT();

/* The string. */

const char string[] TEXT_SEGMENT = "Hello World!\r\n";

/* This will be our entry point */

void NAKED _start()           
{
  int i;
  r32_t eax SET_EAX;

  eax = 0x0;                    
  set_h(eax,0x0e);
  
  i = 0;                   
  do                            
    {                                                                              
      set_l(eax, string[i] );
      bios_int(0x10);
      i++;                    
    }                                                                              
  while ( get_l(eax) != 0x0);   
  
  while(1);

}

MBR_END();

/*  Notes.

    This program is mbr-11b.c as people see it within the Matrix.

 */

