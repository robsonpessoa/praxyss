/*    Hello World in C with inline assembly.

      Like mbr-6a.c but with AT&T assembly for GAS.
*/

extern const char here[];


__asm__(".code16");         /* Set 16-bit code */

/* This will be our entry point */

void __attribute__ ((naked)) start()           
{

  /* Since GCC is not good at handling real mode, we will manipulate 32-bit 
     registers as done in mbr-06a.c 
  */
  

  register volatile int eax __asm__  ("eax");                                      
  register volatile int ebx __asm__  ("ebx");                                      
                                                                                   
  eax = 0x0;                    
                                                                                   
  eax = 0x0e00;                 /* Load 0xe into ah. */                            
                                                                                   
  ebx = 0x0;                    /* Offest to the string. */                        
                                                                                   
  do                            
    {                                                                              
      eax &= 0xffffff00;        /*     prepare for the next line   */              
      eax |= (char) here[ebx];  /*     mov al, BYTE [here + bx]    */              
      __asm__("int     $0x10"); /*     int 0x10                    */              
      ebx++;                    /*     add bx, 0x1                 */              
                                                                                   
    }                                                                              
  while ( (eax & 0x000000ff)   != 0x0);      /*  while(ah != 0x0)  */    
  
  while(1);

}

const char here[]  = "Hello world!\r\n";




__asm__(". = start + 510");                /* Pad with zeros */
__asm__(".byte 0x55, 0xaa");                /* Boot signature  */



/*  Notes.

    This code does not compile with the spcified build rule (Makefile).

    Diversily from what was done with mbr-06*.c, whose intermediate assembly
    code was manually edited, in the present case the the assembly code is fed
    directly into the assembler (GAS). 

    Consulting the generated mbr-10b1.s assembly output, one may see that 
    rather than in the .text segment, the string was placed by the compiler
    in .rodata segment. Since the label 'start' is relative to the segment
    to which it belongs (.text), this confuses the assembler.

    One possible solution is suggested in mbr-10a.c

 */

