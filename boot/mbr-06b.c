 /*	Hello World in C with inline assembly.
	
	An attempt to directly mape 16-bit registers into variables.
*/

extern const char here[];

__asm__("org 0x7c00");		/* Load address. */
__asm__("bits 16");		/* Load address. */

/* In C, our procedure goes within a function. */

void __attribute__ ((naked)) start(void)
{

  /* GCC allows us to tell that a variable should be allocated in a register
     rather than in main memory; and even permits choosing the register. 
     The keyword 'register' before the variable declaration and the assembly
     code after it serve this purpose. The keyword 'volatile' tells the
     compiler that the value of the variable may change asynchronously;
     this tip may be used by the compiler to try to avoid potential troubles.
*/
  
  register volatile char  al __asm__  ("al");
  register volatile char  ah __asm__  ("ah");
  register volatile short bx __asm__  ("bx");

  ah = 0xe;			/* Configure BIOS mode */
  bx = 0;			/* Offest to the string. */

  do
    {				/* The code bellow __should__ be equivalent to*/
      
      al = here[bx];		/*     mov al, [here + bx]                   */
      __asm__("int     0x10");	/*     int 0x10                              */
      bx++;			/*     add bx, 0x1                           */

    }                           /*     cmp al, 0x0 */
  while (al != 0x0);		/*     jne (begining of the block) */

  while(1);			/* Jump forever. */
}

				
const char here[] = "Hello world!\r\n";

/* For now, let's use inline assembly. Later on, we'll see an 
   alternative, neater, way of achieving the same result. */

__asm__("times 510 - ($-$$) db 0"); 
__asm__("dw 0xaa55");	         


/* Notes.

   Unfortunately, this attempt does not work, and due to two main resons. 
   First, even though we can ask the C compiler to generate intel syntax,
   GCC targets the GNU Assembler (GAS) and it so happens that GAS dialect 
   is not compatible to NASM. Therefore, while the inline NASM assembly 
   directives are understood by NASM, the GCC generated C-to-assembly  
   code is not. The result is a mix of two icompatible dialects and both 
   assemblers, NASM and GAS, will fail to compile it for the same reason.

   As a tentative workaround, we may try some ad hoc NASM-to-GAS translation.
   The provided Makefile script does it.

   Nevertheless, even so, the resulting binary remains inadequate due to
   the second problem: GCC is not well-suited to generate 16-bit assembly
   code as we expect in this example. While the flag -m16 does tell the
   compiler to target 16-bit platform, GCC actually outputs assembly code
   that makes use of 32-bit registers and instructions, and it relies on
   GAS's own capability of generating proper 16-bit suited machine code.

   As it's possible to see in the resulting assembly, although we declared
   variables corresponding to 8-bit registers ah and al, respectively, 
   GCC actually uses 32-bit register eax; worse, it does not manipulate
   it according to what was intended in the C source code.
   
   To make a long story short, both mixing NASM inline assembly with C
   and writing 16-bit source code does not go well with GCC. Rather,
   it's advisable to reason based on 32-bit assembly and switch to 
   GAS assembly. That's what we do next: a) mbr-07.s is a rewriting of
   mbr-05.c using GAS-suited AT&T dialect ; and b) mbr-08.c is a 
   translation of mbr-07.s in C with inline AT&T assembly, which
   (hopefully) works. */
