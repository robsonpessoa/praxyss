/*    Hello World in C with inline assembly.

      Like mbr-10.c, rewritten with for a neater source code.
*/

/* Assembly header. */

__asm__(".code16");         

/* The string. */

const char here[] __attribute__((section(".text#"))) = "Hello World!\r\n";

/* This will be our entry point */

void __attribute__ ((naked)) start()           
{
  register volatile int eax __asm__  ("eax");                                      
  register volatile int ebx __asm__  ("ebx");                                      

  eax = 0x0;                    
                                                                                   
  eax = 0x0e00;                 /* Load 0xe into ah. */                            
                                                                                   
  ebx = 0x0;                    /* Offest to the string. */                        
                                                                                   
  do                            
    {                                                                              
      eax &= 0xffffff00;        /*     prepare for the next line   */              
      eax |= (char) here[ebx];  /*     mov al, BYTE [here + bx]    */              
      __asm__("int     $0x10"); /*     int 0x10                    */              
      ebx++;                    /*     add bx, 0x1                 */              
                                                                                   
    }                                                                              
  while ( (eax & 0x000000ff)   != 0x0);      /*  while(ah != 0x0)  */    

  while(1);

}

/* Assembly footer. */

__asm__(". = start + 510");                
__asm__(".byte 0x55, 0xaa");               



/*  Notes.

    This program is mbr-10.c rewritten with the string definition moved to the
    top of the file. Since we're writing a C source code, we expect it to be
    possible.

    A caveat is, however, in order. 

      If we just move the string from where it is in mbr-10a.c (at the bottom) to
      the top, we may run into trouble. This is because the assembly code
      corresponding to the string declaration will carry along with itself
      an entry '.section .text'. There is already an entry of this kind 
      introduced by the compiler, a few lines behind. It's not impossible
      to have more than one text segment. The problem is that the label
      '_start' will fall within this second text segment and, as result,
      it will be the offset from the start of the second text segment and
      not from the beginning of whole the binary file. To circumvent this
      problem, we created a new label 'begin' before anything else. The
      zero-padding entry now uses 'begin' instead of '_start'.

      Put short, the program does not work.

    Yes...  life at system level is cruel (see mbr-11a.c).
      
    "You take the blue pill, the story ends, you wake up in your bed and 
    believe whatever you want to believe. You take the red pill—you stay 
    in Wonderland, and I show you how deep the rabbit hole goes. 
    Remember: all I'm offering is the truth. Nothing more." (Morpheus)

 */

