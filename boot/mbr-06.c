 /*	Hello World in C with inline assembly.
	
	Like mbr-06.c, but manipulating 32-bit register variables, instead. 
*/

extern const char here[];

__asm__("org 0x7c00");		/* Load address. */
__asm__("bits 16");		/* Set 16-bit code. */

/* In C, our procedure goes within a function. */

void __attribute__ ((naked)) start(void)
{

  /* In mbr-06.c we've tried to declare variables to 16-bit registers and,
     as we've seen, it's turned out flawed because of GGC's limitations. 
     A viable workaround is to access 16 and 8-bit register indirectly 
     by means of manipulating the 32-bit registers with bitwise operations.*/
  
  register volatile int eax __asm__  ("eax");
  register volatile int ebx __asm__  ("ebx");

  eax = 0x0;			/* Zero-initialize eax. */
  
  eax = 0x0e00;			/* Load 0xe into ah. */
  
  ebx = 0x0;			/* Offest to the string. */

  do				/* Equivalent instructions */
    {				
      eax &= 0xffffff00;	/*     prepare for the next line   */
      eax |= (char) here[ebx];	/*     mov al, BYTE [here + bx]    */
      __asm__("int     0x10");	/*     int 0x10                    */
      ebx++;			/*     add bx, 0x1                 */

    }                           
  while ( (eax & 0x000000ff)   != 0x0);	     /*  while(ah != 0x0)  */

  while(1);			/* Jump forever.                    */
}

				
const char here[] = "Hello world!\r\n";

/* For now, let's use inline assembly. Later on, we'll see an 
   alternative, neater, way of achieving the same result. */

__asm__("times 510 - ($-$$) db 0"); 
__asm__("dw 0xaa55");	         


/* Notes.

   Except by the NASM-GAS dialect incompatibility, register manipulation
   now works. The provided Makefile script does this.

   Even so, it remains that mixing NASM inline assembly with C and writing 
   16-bit source code does not go well with GCC. Rather, it's advisable to 
   switch to  GAS assembly. That's what we do next: a) mbr-07.s is a rewriting
   of mbr-05.c using GAS-suited syntax (aimed at building familiarity
   with AT&T assembly); and b) mbr-10.c is a translation of mbr-6a.c (this 
   file) in C with inline AT&T assembly, which (hopefully) will work. */
