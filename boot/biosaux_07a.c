
	global _bioswrite
	
	section .data
	
	eaxbkp  dd (0x07c00+512)
	ebxbkp  dd (0x07c00+512)

_bioswrite:
	mov [eaxbkp], ax
	mov [ebxbkp], bx

loop:	
	mov al, [bx]	; Offset + load address
	int 0x10
	add bx, 0x1		; Increment offset
	cmp al, 0x0
	jne loop

end:	
	mov eax, [eaxbkp]
	mov ebx, [ebxbkp]
	ret
