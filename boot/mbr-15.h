#ifndef MBR_H
#define MBR_H


#define bios_int(interrupt) __asm__("int $" #interrupt)
#define set_l(r32,val) (r32 =  ((r32 & 0xffffff00) |  (val & 0x000000ff)))
#define set_h(r32,val) (r32 = ((r32 & 0xffff00ff) | ((val<<8) & 0x0000ff00)))
#define get_l(r32) (r32 & 0x000000ff)
#define get_h(r32) ((r32 & 0x0000ff00) >> 8)


#define NAKED __attribute__ ((naked))

#define r32_t register volatile int 

#define SET_EAX __asm__ ("eax")
#define SET_EBX __asm__ ("ebx")
#define SET_ECX __asm__ ("ecx")
#define SET_EDX __asm__ ("edx")

#define halt() __asm__("hlt")

int main();


#endif	/* MBR_H */

