/*    Finally, our Hello World bootloader in plain C.

      Delight yourself in the supreme elegance of C! Bend the knees in 
      joyful admiration for its breathtaking majestic combination of 
      power and simplicity... Blessed those who can C it!  
*/


#include "mbr-15.h"

int a = 1;

/* The string. */

const char foo[]  = "Hello World!\r\n";

/* A 'print' function */

void  print (const char *str)
{
  int i;
  r32_t eax SET_EAX;
  
  eax = 0x0;
  set_h(eax,0x0e);
  
  i = 0;
  while (str[i] != 0)
    {
      set_l(eax, str[i] );
      bios_int(0x10);
      i++;
    }

}

void nested(const char *smth)
{
  print (smth);
}


/* This will be our entry point */

int main(void)           
{

  print (foo);
  print ("Ain't cool?\r\n");
  nested("Hail yeah!");
  halt();

  return 0;
}


/*  Notes.

    In this code we take advantage of everything we built so far and add some
    extras. Now we have a traditional 'int main()' function as the entry point
    and can define and call functions that encapsulate the low-level jobs
    (which, also, can be written in C). All the inline assembly and bit-wise 
    trickery is hidden in the header file.

    Recall that this is a flat binary executable. Contrary to what we can do
    with a structured executable, such as ELF, we don't have the facilities to
    specify the entry point. Upon booting, the program will start executing 
    from start to end. In order to be able to assign 'main' as the entry
    point we use the auxiliary code mbr-15_start.c.

    We relaxed the 'naked' attribute so that we can pass arguments to
    functions (otherwise we prevent the compiler from generating the code
    necessary to this end). Resulting assembly may be a bit clumsy to read
    and requires concepts of API and calling convention which will be
    subject of future discussions.
    
    This code should be capable of boot and run bare-metal on your standard PC.
    If you've come through all the examples up to this point, you perfectly
    understand any single bit (pun intended) of it. We started by writing bare
    machine code directly, then assembly, and now we are coding in C with
    small portions of inline assembly. No Operating System kernel, no system 
    libraries. Real hardware, harsh raw x86. 

    Cool ah?
	 
 */

