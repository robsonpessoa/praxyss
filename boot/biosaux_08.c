

void __attribute__ ((fastcall)) write (char* a)
{
  char *str = a;
  
  /* GCC allows us to tell that a variable should be allocated in a
     register rather than in main memory; and even permits choosing
     the register. Since direct access to al, ah seems not work (GCC is not
     that good with 16bit real mode), let's try somethind different. */
  
  register volatile short eax __asm__  ("eax");
  register volatile short ebx __asm__  ("ebx");

  ebx=0;
  do
    {				/* The code bellow should be equivalent to */
      eax = str[ebx];	        /*   mov al, [here + bx]                   */
      eax &= 0x00FF;            /* Zero ah                                 */
      eax |= 0x0e00;		/* Configure BIOS mode in ah               */
      __asm__("int $0x10");	/*   int 0x10                              */
      ebx++;			/*   add ebx, 0x1                          */
    }                           /* If ah==0xe && al==0                     */
  while (eax != 0x0e00);	/*   jne (begining of the block)           */

}
