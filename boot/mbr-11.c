/*    Hello World in C with inline assembly.

      Like mbr-11a.c; using proprocessor to separate inline assembly from C.
*/

/* Macros to set and get 8 bit registers. */

#define bios_int(interrupt) __asm__("int $" #interrupt)
#define set_l(r32,val) (r32 =  ((r32 & 0xffffff00) |  (val & 0x000000ff)))
#define set_h(r32,val) (r32 = ((r32 & 0xffff00ff) | ((val<<8) & 0x0000ff00)))
#define get_l(r32) (r32 & 0x000000ff)
#define get_h(r32) ((r32 & 0x0000ff00) >> 8)

/* Assembly header. */

__asm__("begin:");				\
__asm__(".code16");         

/* The string. */

const char string[] __attribute__((section(".text#"))) = "Hello World!\r\n";

/* This will be our entry point */

void __attribute__ ((naked)) _start()           
{
  register volatile int eax __asm__  ("eax");                                      
  register volatile int ebx __asm__  ("ebx");                                      

  eax = 0x0;                    
  ebx = 0x0;                    
                                                                                   
  set_h(eax,0x0e);
  
  do                            
    {                                                                              
      set_l(eax, string[ebx] );
      bios_int(0x10);
      ebx++;                    
                                                                                   
    }                                                                              
  while ( get_l(eax) != 0x0);   
  
  while(1);

}

/* Assembly footer. */

__asm__(". = begin + 510");                
__asm__(".byte 0x55, 0xaa");               



/*  Notes.

    This program is mbr-10a.c rewritten with for a neater source code, more
    concise and easy to read. Improvements include the following changes.

    - Renamed function start() to _start(). Linker ld will look for this label
      by default, so that we don't need to explicitly tell it the liker.

    - Renamed array 'here' to 'string' for a more intuitive denomination.

    - Moved string declaration to the top. A caveat is in order here. If we
      just move the string from where it is in mbr-10a.c (at the bottom) to
      the top, we may run into trouble. This is because the assembly code
      corresponding to the string declaration will carry along with itself
      an entry '.section .text'. There is already an entry of this kind 
      introduced by the compiler, a few lines behind. It's not impossible
      to have more than one text segment. The problem is that the label
      '_start' will fall within this second text segment and, as result,
      it will be the offset from the start of the second text segment and
      not from the beginning of whole the binary file. To circumvent this
      problem, we created a new label 'begin' before anything else. The
      zero-padding entry now uses 'begin' instead of '_start'.

    - We created some macros to make it easier to assign and read values
      from 8-bit portions of the 32-bit registers. Bear in mind that, for
      normal variables, we could have used pointers to get specific bytes
      of a multibyte value. However, our 32-bit variables are registers and
      we can't user pointers.

      Yes...  life used to seem so much simpler for an average OS user.
      
      "You take the blue pill, the story ends, you wake up in your bed and 
      believe whatever you want to believe. You take the red pill—you stay 
      in Wonderland, and I show you how deep the rabbit hole goes. 
      Remember: all I'm offering is the truth. Nothing more." (Morpheus)

 */

