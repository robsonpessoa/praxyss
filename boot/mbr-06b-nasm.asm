	org 0x7c00
	BITS 16
start:
	mov	eax, 14
	mov	ebx, 0
.L2:
	mov	eax, ebx
	cwde
	movzx	eax, BYTE [here + eax]
	int     0x10
	mov	edx, ebx
	add	edx, 1
	mov	ebx, edx
	test	al, al
	jne	.L2
.L3:
	jmp	.L3
here:
	db	"Hello world!\r\n"
	times 510 - ($-$$) db 0
	dw 0xaa55
