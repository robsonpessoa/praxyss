	;; Boot and loop forever.
	;; 
	
	bits 16			; Set 16-bit mode
	
loop:
	jmp loop		; Infinite loop

	times 510 - ($-$$) db 0	; Pad with zeros

	dw 0xaa55		; Boot signature


	;; Notes
	;; 
	;; This assembly source code is written for x86 architecture in intel 
	;; syntax and NASM  assembler dialect. It's mean to be compiled with 
	;; NASM assembler.
	;; 
	;; A label (such as 'loop:') is interpreted by the prepossessesor as
	;; the offset of (byte count at) the next command (jmp instruction, 
	;; in this example).
	;;
	;; The argument of jmp instruction, here, is a relative offset. In
	;; this example, 'jmp loop' is equivalent to 'jmp 0x0'. See (1) in
	;; file notes.txt for further clarifications on the resulting
	;; machine code (mbr-01.hex).
	;;
	;; The directive 'times X Y' produces a sequence of X repetitions of
	;; of Y. The type specification 'db' means that Y is a byte (8 bits).
	;; If it were dw, it would mean 'word', i.e. 16 bits.
	;;
	;; Symbol $  denotes the address of (byte count at) the current line.
	;; Symbol $$ denotes the address of (byte count at) the start of current
	;; section (in present case, we have only one section). Therefore, the
	;; value ($-$$) is the current address minus the address of the
	;; program start. We need 510 minus this amount of zeros.
	;;
	;; The line dw causes the output of the 2-byte pattern for the boot
	;; signature at the current position (positions 511 and 512).


	

	

	
	
