#include "mbr-15.h"

__asm__("call _start");		       /* Casll start. */

void __attribute__((naked))_start()    /* Starts call main. */
{
  main();
}


/* Notes.

   This startup code must be the first bytes in the main program's binary file.
   It's important because, differently from a regular elf file, we can't specify
   an entry point in a flat binary executable. Upon booting, it will execute from
   begining to end as it is.

*/
